# Introduction
This is a system to manage users in a framework agnostic way.

# Install
```
npm install
npm run clear && node ./scripts/create_admin admin password
npm start
```

# Documentation

## Specs
```
# Manage users
GET       /me                          # get info about the current user
PUT       /user                        # create a new user: username=email,password=string
DELETE    /user/:username              # delete a user

## Password reset process
GET       /user/mail/activate/:token   # activate a user
GET       /user/mail/reset/:email      # reset user password

## Authorizations (Oauth2)
GET       /oauth/authorize
POST      /oauth/authorize
POST      /oauth/token

## Admin application
GET       /admin/users                 # list all users
DELETE    /admin/user/:username        # delete a user
POST      /admin/user/:useranme        # update user
GET       /admin/application           # list all applications
DELETE    /admin/application/:username # delete a user
POST      /admin/application/:useranme # update user
```

## Users Authorization
User authorization implements the Oauth2 spec.

## Admin
An admin can create applications and manage their users.

```
## Authorization
GET       /oauth/authorize/:code
GET       /oauth/authorize
POST      /oauth/authorize
DELETE    /oauth/authorize
POST      /oauth/token
POST      /oauth/revoke
resources /oauth/applications
GET       /oauth/authorized_applications
DELETE    /oauth/authorized_applications/:id
GET       /oauth/token/info
```



# Guides
## Authorization Code grant
## Password grant
curl http://localhost:9999/oauth/token -d 'grant_type=password&username=username@gmail.com&password=user_password&client_id=app_name0&client_secret=app_secret'
## Authorization Code
weird errors
curl http://localhost:9999/oauth/token -d 'grant_type=authorization_code&username=user_name&client_id=app_name&client_secret=app_secret&code=test'
## Client Credentials
curl http://localhost:9999/oauth/token -d 'grant_type=client_credentials&username=user_name&password=user_pass&client_id=app_name&client_secret=app_secret'
## Refresh Token
curl http://localhost:9999/oauth/token -d 'grant_type=refresh_token&client_id=app_name&client_secret=app_secret&refresh_token='


# Sqlite
## Schema
```
CREATE TABLE users (
  id uuid NOT NULL,
  username TEXT NOT NULL,
  password TEXT NOT NULL
)


CREATE TABLE oauth_clients (
    client_id text NOT NULL,
    client_secret text NOT NULL,
    redirect_uri text NOT NULL
);

CREATE TABLE oauth_access_tokens (
    access_token text NOT NULL,
    client_id text NOT NULL,
    user_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);

CREATE TABLE oauth_refresh_tokens (
    refresh_token text NOT NULL,
    client_id text NOT NULL,
    user_id uuid NOT NULL,
    expires timestamp without time zone NOT NULL
);
```
