var user = require('./utils/user'),
    q = require('q'),
    reset = require('../../scripts/reset');

var users_list = new Array(5).fill(0).map(function(el, n){
    return {
	username: 'user_'+n+'@gmail.com',
	password: 'password_'+n
    }
})

describe('CRUD user::', function(){
    describe('creation', function(){
	beforeEach(function(done){
	    reset().then(e=> done())
	})

	it('creates a user', function(done){
	    user.create_user({username:'jean@gmail.com', password:'passw0rd'})
		.then(e=> done())
	});
	it('creates a bunch of users', function(done){
	    q.all(users_list.map(function(_usr){
		return user.create_user(_usr)
	    })).then(e=> done()).catch(function(err){console.log(err)})
	});

	it('requires valid email', function(done){
	    user.create_user({username: 'test@gmail.', password: 'passw0rd'}).catch(function(){
		done()
	    })
	});

	it('requires valid password', function(done){
	    user.create_user({username: 'test@gmail.com', password: ''}).catch(function(){
		done()
	    })
	});

	it('can\'t register multiple times', function(done){
	    user.create_user(users_list[0]).then(function(){
		user.create_user(users_list[0]).catch(function(){
		    done()
		})	
	    })
	});
    });
})
