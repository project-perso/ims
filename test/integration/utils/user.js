var q = require('q'),
    app = require('../../../src/main'),
    db = require('../../../src/lib/db'),
    request = require('supertest');

module.exports.create_user = function(data, opt){
    if(!opt) opt = {verify: true};
    return request(app)
	.put('/user')
	.send(data)
	.expect(200)
	.then(function(res){
	    res = res.body;

	    if(res.status !== 'ok'){
		return q.reject(res);
	    }else if(opt.verify){
		return _verify(res.result)
	    }else{
		return q.resolve();
	    }	   
	});
}

function _verify(res){
    return fetch(res.username, res.password)
	.then(verify)
    
    function fetch(username, password){
	return db.query("SELECT * FROM users WHERE username = $1", [username])
	    .then(function(rows){
		return q.when({
		    username: username,
		    password: password,
		    rows: rows
		})
	    })
    }
    function verify(data){
	if(data.rows.length !== 1){
	    return q.reject(data.rows.length+' users in the db')
	}
	var user = data.rows[0];
	if(user.username === data.username){
	    return q.when()
	}
	return q.reject('invalid user')
    }
}
