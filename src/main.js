var express = require('express'),
    utils = require('./lib/utils'),
    app = express();


utils.setup(app);

app.use('/user', require('./routes/user'));
app.use('/app', require('./routes/app'));
app.use('/me', require('./routes/me'));
app.use('/oauth', require('./routes/oauth'));

app.listen(9999, function(err, t){
    if(err) console.log(err)
    else console.log('listening')
});


module.exports = app;
