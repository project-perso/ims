var authCode = require('./oauth_authcode'),
    client = require('./oauth_client'),
    accessToken = require('./oauth_accesstoken'),
    refreshToken = require('./oauth_refreshtoken'),
    user = require('./user');

module.exports.getAuthCode = authCode.getAuthCode;
module.exports.saveAuthCode = authCode.saveAuthCode;
module.exports.getAccessToken = accessToken.getAccessToken;
module.exports.saveAccessToken = accessToken.saveAccessToken;
module.exports.saveRefreshToken = refreshToken.saveRefreshToken;
module.exports.getRefreshToken = refreshToken.getRefreshToken;
module.exports.getUser = user.getUser;
module.exports.getClient = client.getClient;
//module.exports.grantTypeAllowed = client.grantTypeAllowed;
