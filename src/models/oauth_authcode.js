var q = require('q'),
    _db = require('../lib/db');

// AUTHORIZATION_CODE
// bug: https://github.com/oauthjs/node-oauth2-server/blob/cd7776ce19b4e55a0e5800d78e4186c741711b36/lib/grant-types/authorization-code-grant-type.js#L173
module.exports.getAuthorizationCode = function(code){
    console.log('> get auth code')
    var authorization = db.authorizations.filter(function(auth){
	return auth.code === code
    });
    if(authorization.length !== 1) return false
    var auth = authorization[0],
	user = db.users[auth.user_id],
	client = db.clients[auth.client_id];
    if(user && client){
	console.log('> get authorization code')
	console.log({
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    timestamp: auth.expiresAt - new Date(),
	})
	return {
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    grants: GRANTS,
	}
    }
    return false;   
}
module.exports.revokeAuthorizationCode = function(old_auth){
    console.log('> revoke auth code')
    db.authorizations = db.authorizations.filter(function(auth){
	if(auth.authorizationCode === old_auth.authorizationCode) return false
	return true;
    })
    return old_auth;
}
module.exports.saveAuthorizationCode = function(code){
    console.log('> save auth code')
    console.log(code)
    db.authorizations.push(code)
}
module.exports.generateAuthorizationCode = function(a){
    console.log('> generate auth code')
    console.log(a)
    return 'foo'
}
