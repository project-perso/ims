var q = require('q'),
    _db = require('../lib/db');

module.exports.saveToken = function(token, client, user) {
    console.log('> save token')
    _db.run("INSERT INTO oauth_access_tokens(access_token, client_id, user_id, expires) VALUES($1,$2,$3,$4)", [
	token.accessToken,
	client.clientId,
	user.id,
	token.accessTokenExpiresAt
    ])
    _db.run("INSERT INTO oauth_refresh_tokens(refresh_token, client_id, user_id, expires) VALUES($1,$2,$3,$4)", [
	token.refreshToken,
	client.clientId,
	user.id,
	token.refreshTokenExpiresAt
    ])
    var token = {
	accessToken: token.accessToken,
	accessTokenExpiresAt: token.accessTokenExpiresAt,
	refreshToken: token.refreshToken,
	refreshTokenExpiresAt: token.refreshTokenExpiresAt,
	client: client.clientId,
//	scope: token.scope,
	user: user.id
    };
    db.tokens.push(token)
    return q.when(token);
};



module.exports.revokeToken = function(bearerToken){
    console.log('> revoke token')
    console.log(bearerToken)
    return _db.run("DELETE FROM access_token WHERE access_token = $1", [bearerToken])
	.then(function(){
	    return q.when(bearerToken)
	})
}
