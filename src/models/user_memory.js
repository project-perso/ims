var db = {
    clients: {
	'app_name0': {
	    clientId: 'app_name0',
	    clientSecret: 'app_secret',
	    redirectUris: ['uri']
	}
    },
    users: {
	'username@gmail.com':{
	    id: 'username@gmail.com',
	    username: 'username@gmail.com',
	    password: 'user_password',
	    apps: ['app_name'],
	}
    },
    tokens: [],
    authorizations: [
	{code: 'test', user_id:'username@gmail.com', client_id: 'app_name0', expiresAt: new Date(new Date().getTime() + 1000*30)} // default has to be less than 5 minutes
    ]    
};

const GRANTS = ['password', 'refresh_token', 'authorization_code', 'client_credentials']

module.exports.getAccessToken = function(bearerToken) {
    console.log('> get access token')
    console.log(bearerToken)
    var tokens = db.tokens.filter(function(token) {
	return token.accessToken === bearerToken;
    });
    return tokens.length ? tokens[0] : false;
};

module.exports.getClient = function(clientId, clientSecret) {
    var client = db.clients[clientId]
    if(client && client.clientSecret === clientSecret){
	return {
	    clientId: clientId,
	    clientSecret: clientSecret,
	    grants: GRANTS
	}
    }
    return false;
};

module.exports.getUser = function(username, password) {
    var user = db.users[username];
    if(user && user.password === password){
	return user;
    }
    return false
};

module.exports.getRefreshToken = function(bearerToken) {
    console.log('> get resfresh token')
    console.log(bearerToken)
    var tokens = db.tokens.filter(function(token) {
	return token.refreshToken === bearerToken;
    });
    return tokens.length ? tokens[0] : false;
};

module.exports.saveToken = function(token, client, user) {
    console.log('> save token')
    console.log(token)
    var token = {
	accessToken: token.accessToken,
	accessTokenExpiresAt: token.accessTokenExpiresAt,
	refreshToken: token.refreshToken,
	refreshTokenExpiresAt: token.refreshTokenExpiresAt,
	scope: token.scope,
	client: client.clientId,
	scope: token.scope,
	user: user.id
    };
    db.tokens.push(token)
    return token
};

module.exports.validateScope = function(a, b, c){
    console.log(a)
    return 'foo'
}



// REFRESH_TOKEN
module.exports.revokeToken = function(bearerToken){
    console.log('> revoke token')
    console.log(bearerToken)
    db.tokens = db.tokens.filter(function(token){
	//return token.refreshToken === token? false : true;
	return true
    })
    return bearerToken;
}

// AUTHORIZATION_CODE
// bug: https://github.com/oauthjs/node-oauth2-server/blob/cd7776ce19b4e55a0e5800d78e4186c741711b36/lib/grant-types/authorization-code-grant-type.js#L173
module.exports.getAuthorizationCode = function(code){
    console.log('> get auth code')
    var authorization = db.authorizations.filter(function(auth){
	return auth.code === code
    });
    if(authorization.length !== 1) return false
    var auth = authorization[0],
	user = db.users[auth.user_id],
	client = db.clients[auth.client_id];
    if(user && client){
	console.log({
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    timestamp: auth.expiresAt - new Date(),
	})
	return {
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    grants: GRANTS,
	}
    }
    return false;   
}
module.exports.revokeAuthorizationCode = function(old_auth){
    console.log('> revoke auth code')
    db.authorizations = db.authorizations.filter(function(auth){
	if(auth.authorizationCode === old_auth.authorizationCode) return false
	return true;
    })
    return old_auth;
}
module.exports.saveAuthorizationCode = function(code){
    console.log('> save auth code')
    console.log(code)
    db.authorizations.push(code)
}
module.exports.generateAuthorizationCode = function(a){
    console.log('> generate auth code')
    console.log(a)
    return 'foo'
}

// CLIENT_CREDENTIALS
module.exports.getUserFromClient = function(client){
    return db.users['user_name']
}
