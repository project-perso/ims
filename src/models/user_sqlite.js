var q = require('q'),
    _db = require('../lib/db');

var db = {
    clients: {
	'app_name0': {
	    clientId: 'app_name0',
	    clientSecret: 'app_secret',
	    redirectUris: ['uri']
	}
    },
    users: {
	'username@gmail.com':{
	    id: 'username@gmail.com',
	    username: 'username@gmail.com',
	    password: 'user_password',
	    apps: ['app_name'],
	}
    },
    tokens: [],
    authorizations: [
	{code: 'test', user_id:'username@gmail.com', client_id: 'app_name0', expiresAt: new Date(new Date().getTime() + 1000*30)} // default has to be less than 5 minutes
    ]    
};

const GRANTS = ['password', 'refresh_token', 'authorization_code', 'client_credentials']

module.exports.getAccessToken = function(bearerToken) {
    console.log('> get access token')
    console.log(bearerToken)
    var tokens = db.tokens.filter(function(token) {
	return token.accessToken === bearerToken;
    });
    return tokens.length ? tokens[0] : false;
};

module.exports.getClient = function(clientId, clientSecret) {
    var client = db.clients[clientId]
    if(client && client.clientSecret === clientSecret){
	return {
	    clientId: clientId,
	    clientSecret: clientSecret,
	    grants: GRANTS
	}
    }
    return false;
};

module.exports.getUser = function(username, password) {
    var user = db.users[username];
    console.log('> get user')
    return _db.query("SELECT * FROM users WHERE username = $1", username)
	.then(function(rows){
	    if(user && user.password === password){
		return q.when(user);
	    }
	    return q.when(false)
	})
};

module.exports.getRefreshToken = function(bearerToken) {
    console.log('> get resfresh token')
    console.log(bearerToken)
    _db.query("SELECT * from oauth_access_tokens WHERE access_token = $1", bearerToken)
	.then(function(rows){
	    console.log(rows)
	})
    var tokens = db.tokens.filter(function(token) {
	return token.refreshToken === bearerToken;
    });
    return tokens.length ? tokens[0] : false;
};

module.exports.saveToken = function(token, client, user) {
    console.log('> save token')
    _db.run("INSERT INTO oauth_access_tokens(access_token, client_id, user_id, expires) VALUES($1,$2,$3,$4)", [
	token.accessToken,
	client.clientId,
	user.id,
	token.accessTokenExpiresAt
    ])
    _db.run("INSERT INTO oauth_refresh_tokens(refresh_token, client_id, user_id, expires) VALUES($1,$2,$3,$4)", [
	token.refreshToken,
	client.clientId,
	user.id,
	token.refreshTokenExpiresAt
    ])
    var token = {
	accessToken: token.accessToken,
	accessTokenExpiresAt: token.accessTokenExpiresAt,
	refreshToken: token.refreshToken,
	refreshTokenExpiresAt: token.refreshTokenExpiresAt,
	client: client.clientId,
//	scope: token.scope,
	user: user.id
    };
    db.tokens.push(token)
    return q.when(token);
};

module.exports.validateScope = function(a, b, c){
    return 'foo'
}



// REFRESH_TOKEN
module.exports.revokeToken = function(bearerToken){
    console.log('> revoke token')
    console.log(bearerToken)
    return _db.run("DELETE FROM access_token WHERE access_token = $1", [bearerToken])
	.then(function(){
	    return q.when(bearerToken)
	})
}

// AUTHORIZATION_CODE
// bug: https://github.com/oauthjs/node-oauth2-server/blob/cd7776ce19b4e55a0e5800d78e4186c741711b36/lib/grant-types/authorization-code-grant-type.js#L173
module.exports.getAuthorizationCode = function(code){
    console.log('> get auth code')
    var authorization = db.authorizations.filter(function(auth){
	return auth.code === code
    });
    if(authorization.length !== 1) return false
    var auth = authorization[0],
	user = db.users[auth.user_id],
	client = db.clients[auth.client_id];
    if(user && client){
	console.log('> get authorization code')
	console.log({
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    timestamp: auth.expiresAt - new Date(),
	})
	return {
	    authorizationCode: auth.code,
	    user: user,
	    client: client,
	    expiresAt: auth.expiresAt,
	    grants: GRANTS,
	}
    }
    return false;   
}
module.exports.revokeAuthorizationCode = function(old_auth){
    console.log('> revoke auth code')
    db.authorizations = db.authorizations.filter(function(auth){
	if(auth.authorizationCode === old_auth.authorizationCode) return false
	return true;
    })
    return old_auth;
}
module.exports.saveAuthorizationCode = function(code){
    console.log('> save auth code')
    console.log(code)
    db.authorizations.push(code)
}
module.exports.generateAuthorizationCode = function(a){
    console.log('> generate auth code')
    console.log(a)
    return 'foo'
}

// CLIENT_CREDENTIALS
module.exports.getUserFromClient = function(client){
    console.log('> get user from client')
    console.log(client)
    _db.query("SELECT users.id FROM users as u JOIN oauth_client as o ON u.")
    return db.users['username@gmail.com']
}
