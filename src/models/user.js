var q = require('q'),
    _db = require('../lib/db');

module.exports.getUser = function(username, password) {
    console.log('> get user')
    _getUser(username)
	    .then(user => _verifyPassword(user, password))
};


module.exports.register = function(username, password){
    console.log('> register user')
    return _db.run("INSERT INTO users(id, username, password) VALUES($1,$2,$3)",[
	    'id',
	    username,
	    bcrypt.hashSync(password, 3);
    ])
}
module.exports.lost_password = function(username){
    console.log('> lost_password')
    // send an email
}
module.exports.set_password = function(password, security_code){
    console.log('> set_password')
    // TO IMPLEMENT
}


function _getUser(username, password){
    return _db.query("SELECT * FROM users WHERE username = $1", username)
	    .then(function(rows){
	        return rows.length === 1 ? q.when(rows[0]) : q.reject('invalid user');
	    })
}
function _verifyUser(user, password){
    if(!user.active){
	    return q.when('unactive user')
    }else if(bcrypt.compareSync(password, user.hashed_password)){
	    return q.when(user)
    }else{
	    return q.reject('invalid user')
    }
}
