var OauthRouter = require('express').Router(),
    oauthServer = require('express-oauth-server');

var oauth = new oauthServer({
    model: require('../models/user_sqlite'),
    debug: true
});

OauthRouter.post('/token', oauth.token());
OauthRouter.post('/authorize', oauth.authorize());
// OauthRouter.post('/authorize', function(req, res) {    
//     return app.oauth.authorize();
// });
// OauthRouter.get('/authorize', function(req, res, next){
//     if (!req.session || !req.session.user) {
// 	return res.redirect('/login?redirect=' + req.path + '&client_id=' + req.query.client_id + '&redirect_uri=' + req.query.redirect_uri);
//     }
//     res.render('authorise', {
// 	client_id: req.query.client_id,
// 	redirect_uri: req.query.redirect_uri
//     });
// })



module.exports = OauthRouter;
