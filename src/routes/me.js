var meRouter = require('express').Router();

/**
 * Create a new user
 */
meRouter.put('/', function(req, res){
    res.json({
	status: 'ok',
	result: {
	    user_id: null	    
	}
    })
});

meRouter.delete('/', function(req, res){
    res.json({
	status: 'ok',
	result: 'DONE'
    })
});

meRouter.get('/', function(req, res){
    res.send("pong")
});

module.exports = meRouter;
