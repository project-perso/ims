var userRouter = require('express').Router(),
    utils = require('../lib/utils')
    db = require('../lib/db');

/**
 * Create a new user
 */
userRouter.get('/', function(req, res){
    res.send('pong')
})
userRouter.put('/', function(req, res){
    req.assert('username', 'require a valid email address').notEmpty().isEmail();
    req.assert('password').notEmpty();
    var errors = req.validationErrors();
    if(errors){
	res.json(utils.send('oups', errors))
	return 
    }
    req.sanitize('username').toString().trim();
    req.sanitize('password').toString().trim();
    var user = req.body.username,
	password = req.body.password;
    
    db.run("INSERT INTO users(id, username, password) VALUES($1,$2,$3)", [
	utils.hash(),
	user,
	password
    ]).then(function(rows){
	res.json(utils.send({
	    username: user,
	    password: password	    
	}));
    }).catch(function(err){
	res.json(utils.send('oups', err));
    })
})

userRouter.delete('/:email', function(req, res){
    var email = req.checkBody('email').isEmail().toString();    
    db.run("DELETE FROM users WHERE username = $1", [
	email
    ])
})

userRouter.post('/reset_password/:email', function(req, res){
})

userRouter.get('/reset_password', function(req, res){
});

userRouter.post('/password/:secure_token', function(req, res){
})


module.exports = userRouter;
