var appRouter = require('express').Router();

/**
 * Create a new app
 */
appRouter.put('/', function(req, res){
    res.json({
	status: 'ok',
	result: {
	    client_id: null,
	    client_secret: null	    
	}
    });
});


/**
 * Delete an app
 */
appRouter.delete('/:app_id', function(req, res){
    res.json({
	status: 'ok',
	result: 'DONE'
    });
});

/**
 * List apps, current user as access to
 */
appRouter.get('/', function(req, res){
    res.json({
	status: 'ok',
	results: [
	]
    });
});

/**
 * List data associated with the app
 */
appRouter.get('/:client_id', function(req, res){
    res.json({
	status: 'ok',
	result: {
	    app_id: null,
	    users: [
	    ]
	}
    });
});



module.exports = appRouter;
