var uuid = require('node-uuid');

module.exports.hash = function(){
    return uuid.v4()
};


module.exports.setup = function(app){
    // Setup required middleware
    var bodyParser = require('body-parser');
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(require('express-validator')());
//    app.use(require('morgan')('dev'))
    app.set('view engine', 'ejs');

    // setup oauth2
    var oauthServer = require('express-oauth-server');
    app.oauth = new oauthServer({
	model: require('../models/user_memory'),
	debug: true
    });
}


module.exports.send = function(response, error){
    if(error){
	console.error(error)
	return {
	    status: 'error',
	    message: response
	}
    }else{
	if(Array.isArray(response)){
	    return {
		status: 'ok',
		results: response
	    }
	}else{
	    return {
		status: 'ok',
		result: response
	    }
	}
    }
}
