var sqlite = new require('sqlite3'),
    q = require('q'),
    db = new sqlite.Database('users.db');

module.exports.query = function(data, args){
    if(Array.isArray(data)){
	return q.all(data.map(function(_data){
	    if(typeof _data === 'string') return query(_data)
	    else return query(_data.query, _data.args)
	}))
    }else{
	return query(data, args)
    }
    function query(_query, _arg){
	var def = q.defer();
	db.all(_query, _arg, function(err, rows){
	    if(err) return def.reject(err)
	    else return def.resolve(rows)
	})
	return def.promise;
    }
};

module.exports.run = function(data, args){
    if(Array.isArray(data)){
	return q.all(data.map(function(_data){
	    if(typeof _data === 'string') return query(_data)
	    else return query(_data.query, _data.args)
	}))
    }else{
	return query(data, args)
    }
    function query(_query, _arg){
	var def = q.defer();
	db.run(_query, _arg, function(err, rows){
//	    console.log(err)
	    if(err) return def.reject(err)
	    else return def.resolve(rows)
	})
	return def.promise;
    }
};
module.exports.close = function(){
    db.close;
}
