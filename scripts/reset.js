var db = require('../src/lib/db'),
    q = require('q');

function reset(admin_name, admin_password){
    return flush()
	.then(create_schema)
    	.then(create_index)
	.then(e=> create_admin(admin_name, admin_password))
	.then(e => db.close())
    
    function flush(){
	var queries = [
	    "DROP TABLE IF EXISTS users",
	    "DROP TABLE IF EXISTS oauth_client",
	    "DROP TABLE IF EXISTS oauth_access_tokens",
	    "DROP TABLE IF EXISTS oauth_refresh_tokens",
	    "DROP TABLE IF EXISTS oauth_authorization"
	];
	return db.run(queries);
    }
    function create_schema(){
	var queries = [
	    "CREATE TABLE users(id uuid, username VARCHAR(128), password VARCHAR(32), profile TEXT DEFAULT NULL, authorized_services TEXT DEFAULT NULL, active INTEGER DEFAULT NULL, PRIMARY KEY(id))",
	    "CREATE TABLE oauth_client(client_id uuid NOT NULL, client_secret uuid NOT NULL, redirectUris VARCHAR(256), client_name VARCHAR(64), description TEXT, PRIMARY KEY (client_id, client_secret))",
	    "CREATE TABLE oauth_access_tokens(access_token VARCHAR(32) NOT NULL, client_id uuid NOT NULL, user_id uuid NOT NULL, expires timestamp, PRIMARY KEY(access_token))",
	    "CREATE TABLE oauth_refresh_tokens(refresh_token VARCHAR(32) NOT NULL, client_id uuid NOT NULL, user_id uuid NOT NULL, expires timestamp, PRIMARY KEY(refresh_token))",
	    "CREATE TABLE oauth_authorization(auth_code VARCHAR(32) NOT NULL, user_id uuid NOT NULL, client_id uuid NOT NULL, expiresAt timestamp, PRIMARY KEY(auth_code, client_id, user_id))"
	]
	return db.run(queries);
    }
    function create_index(){
	var queries = [
	    "CREATE UNIQUE INDEX user_username_password_index ON users(username)",
	];
	return db.run(queries)
    }
    function create_admin(){
	return q.when('DONE')
    }
}

module.exports = reset;

if (require.main === module) {
    reset()
}
